#!/usr/bin/env python
# -*- coding: utf-8 -*-

import OPi.GPIO as GPIO
from time import sleep          # this lets us have a time delay

GPIO.setboard(GPIO.ONE)    # Orange Pi PC board
GPIO.setmode(GPIO.SOC)        # set up BOARD BCM numbering
sled = GPIO.PA+15
GPIO.setup(sled, GPIO.OUT)         # set BCM7 (pin 26) as an output (LED)

try:
    print ("Press CTRL+C to exit")
    while True:
        GPIO.output(sled, 1)       # set port/pin value to 1/HIGH/True
        sleep(0.1)
        GPIO.output(sled, 0)       # set port/pin value to 0/LOW/False
        sleep(0.1)

        GPIO.output(sled, 1)       # set port/pin value to 1/HIGH/True
        sleep(0.1)
        GPIO.output(sled, 0)       # set port/pin value to 0/LOW/False
        sleep(0.1)

        sleep(0.5)

except KeyboardInterrupt:
    GPIO.output(sled, 0)           # set port/pin value to 0/LOW/False
    GPIO.cleanup()              # Clean GPIO
    print ("Bye.")
